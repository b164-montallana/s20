console.log('Hello');

// JSON Object
//JavaScript Object Notation
//it is a data format

//JSON is used for serializing different data types into bytes

/*
Syntax;

    {
        "propertyA": "ValueA",
        "propertyB": "ValueB",
    }
*/

// {
//     "city": "Quezon City",
//     "province": "Metro Manila",
//     "country": "Philippines"
// }


// // JASON ARRAYs
// {
//     "cities": [
//         {"city": "Quezon City", "province": "Metro Manila", "country": "Philippines"}
//         {"city": "Manila City", "province": "Metro Manila", "country": "Philippines"}
//         {"city": "Makati City", "province": "Metro Manila", "country": "Philippines"}
//     ]
// }


//JSON METHODS

let batchesArr = [
    {
        batchName: 'Batch X'
    },
    {
        batchName: 'Batch Y'
    }
]
//before sending data, use stringify to convert
console.warn('Result from stringify method');
console.log(JSON.stringify(batchesArr));

// other method for stringify
let data = JSON.stringify({
    name: 'Jhon',
    age: 31,
    address: {
        city: 'Manila',
        country: 'Philippines'
    }
});

console.log(data);


let firstName = prompt('What is your first name?');
let lastName = prompt('What is your last name?');
let age = prompt('What is your age?');
let address = {
    city: prompt('What is your city'),
    country: prompt('What is your country?')
}

let profile = JSON.stringify({
    firstName: firstName,
    lastName: lastName,
    age: age,
    address: address
})

console.log(profile);

// 
// 
// 

let batchesJSON = `[
    {
        "batchName": "Batch X"
    },
    {
        "batchName": "Batch y"
    }
]`

console.log(batchesJSON);

// JSON.parse

console.warn('Result from parse method');
console.log(JSON.parse(batchesJSON));